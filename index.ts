/* eslint-disable no-ex-assign */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Context, Hono, MiddlewareHandler, Next } from 'hono';
import { serve } from '@hono/node-server';
import { ServerType } from '@hono/node-server/dist/types';

export * from 'hono';

/*********************************************************************
 * TRouteLite
 * type for route callbacks
 *********************************************************************/ 
export type TRouteLite = (c: Context, extras: any) => any;

/*********************************************************************
 * IRouteOptions
 * options for configuring individual routes
 *********************************************************************/ 
export interface IRouteOptions {
    useApiKey?: boolean;                                // apply API keys to this route?
}

interface IRouteConfig{
    method: string,
    path: string,
    callback: TRouteLite,
    opts?: IRouteOptions,
}

/*********************************************************************
 * IConfigOptions
 * options for configuring the server at large
 *********************************************************************/ 
export interface IConfigOptions {
    apiKeyHeader?: string,                              // request header in which API keys will be stored
    apiKeyValue?: string,                               // value of the API key
    secureAllRoutes?: boolean                           // secure all routes on this server?
    verbose?: boolean                                   // enable verbose logging
}

/**********************************************************************
 * ErrorLite
 * this extends the Error class to specifically handle the types of errors
 * thrown within this server's routes
 **********************************************************************/
export class ErrorLite extends Error{
    readonly id: string;
    readonly err: any;
    readonly status: number;

    constructor(id: string, meta: any, statusCode: number = 500){
        super(id);
        this.id = id;
        this.err = meta instanceof Error? meta.message : meta;
        this.status = statusCode;
    }

    get error(){
        return {
            id: this.id,
            error: this.err,
            status: this.status
        }
    }
}

/***********************************************************************
 * reads the api key in the specified header, validates it against the
 * api key that is provided
 * 
 * @param cxt - Hono context
 * @param header - the request header where the API key resides
 * @param key - value of the API key
 * @returns throws an error if the API key is invalid
 **********************************************************************/
const checkApiKey = (cxt: Context, header: string, key: string) => {
    const keyInReq = cxt.req.header(header);
    if(key && key.trim().length > 0 && key === keyInReq) return true;
    throw new ErrorLite("invalid_api_key", "the api key in your headers is missing or invalid", 401);
}

/***********************************************************************
 * wrapper for the callback that runs the route's business logic
 * @param c - Hono context
 * @param extras - an object that will be delivered the callback
 * @param callback - route callback / business logic
 * @param options - route options
 * @returns the response / error from the execution of the callback
 **********************************************************************/
const execute = async (c: Context, extras: any, callback: TRouteLite, options?: IConfigOptions, afterCb?: any ) => {
    try{
        if(options && options.apiKeyValue && options.apiKeyValue.trim().length > 0){
            const {apiKeyHeader = '', apiKeyValue = ''} = options;
            checkApiKey(c, apiKeyHeader, apiKeyValue);
        }

        const response = await callback(c, extras);
        if(response instanceof ErrorLite) throw response;
        if(typeof afterCb === 'function') afterCb(c, { response });
        return response;
    }
    catch(e: any){
        if(!(e instanceof ErrorLite)) {
            e = new ErrorLite(e?.id || 'error', e?.message || e);
        }
        if(typeof afterCb === 'function') afterCb(c, { error: e });
        throw e;
    }
}

/***********************************************************************
 * validates the config options passed into the server
 * @param port - the port you want to run the server on
 * @param options - ConfigOptions for the server
 * @returns returns the validated options or throws an error
 **********************************************************************/
const validateConfigOptions = (port: number, options?: IConfigOptions) => {
    if(!options) return undefined;

    const { apiKeyValue = undefined, secureAllRoutes = false } = options;
    const { apiKeyHeader = undefined } = options;
    if(apiKeyValue && apiKeyValue.trim().length === 0) throw new Error("invalid_api_key");
    if(apiKeyHeader && apiKeyHeader.trim().length === 0) throw new Error("invalid_api_header");
    if(apiKeyValue && !apiKeyHeader) options.apiKeyHeader = "x-api-key";
    if(apiKeyHeader && !apiKeyValue) throw new Error("api_key_not_defined");
    if(secureAllRoutes && (!apiKeyHeader || !apiKeyValue)) throw new Error("cannot_secure_api");
    if(!port || port <= 0 || port > 65535) throw new Error("invalid_port");

    return options;
}

/***********************************************************************
 * HonoLite
 * the lite wrapper for Hono servers
 **********************************************************************/
export class HonoLite extends Hono {
    private readonly port: number;
    private extras: {[x: string]: any}
    private server: ServerType;
    private configOptions?: IConfigOptions;
    private routeList: Array<IRouteConfig> = [];
    private verbose: boolean = false;
    private _before?: MiddlewareHandler;
    private _after?: (c: Context, r?: unknown) => void;

    /***********************************************************************
     * constructor
     * @param port the port you want to run the server on
     * @param extras an object that will be passed into every route callback
     * @param options server config options
     **********************************************************************/
    constructor(port: number, options?: IConfigOptions, extras = {}){
        super();
        
        this.configOptions = validateConfigOptions(port, options);
        this.port = port;
        this.extras = extras;
        
        const {
            secureAllRoutes = false, 
            apiKeyHeader = '', 
            apiKeyValue = ''
        } = <IConfigOptions>this.configOptions || {};
        
        if(secureAllRoutes === true){
            this.use('*', async (cxt, next) => {
                checkApiKey(cxt, <string>apiKeyHeader, <string>apiKeyValue);
                await next();
            });
        }

        this.onError((error: any, context) => {
            return context.json({...error}, error?.status ?? 500);
        });
        
        this.notFound((context) => {
            return context.json({id: "not_found"}, 404)
        });
    }

    /************************************************************************
     * internal print function based on 'verbose' configuration
     ***********************************************************************/
    private print(message: any, ...optionalParams: any[]){
        if(this.verbose){
            console.log(message, ...optionalParams);
        }
    }

    set before(callback: MiddlewareHandler){
        this._before = callback;
    }

    set after(callback: (c: Context, r?: unknown) => void){
        this._after = callback;
    }

    /***********************************************************************
     * add a key-value pair the 'extras' map
     * @param key key
     * @param value value to add in the extras
     **********************************************************************/
    addExtra(key: string, value: any){
        if(!Object.prototype.hasOwnProperty.call(this.extras, key)) {
            this.extras[key] = value;
        }
    }

    /***********************************************************************
     * setter for the api key
     * will only set it if the initial config options supported this
     **********************************************************************/
    set apiKey(value: string){
        if(!this.configOptions) throw new Error("config_options_unspecified");
        this.configOptions.apiKeyValue = value;
    }

    /***********************************************************************
     * start the server
     * @param callback a callback to invoke after server starts
     * @returns promise
     **********************************************************************/
    start(callback?: (arg: unknown) => void){
        this.extras = Object.freeze(this.extras);

        // attach the pre-request middleware
        if(this._before){
            this.use('*', this._before);
        }

        // attach all the routes
        if(this.routeList.length > 0){
            for(const routeOpts of this.routeList){
                const extras = this.extras;
                const {method, path, callback, opts} = routeOpts;
                const options = opts?.useApiKey ? this.configOptions : undefined;

                if(Object.prototype.hasOwnProperty.call(this, method)){
                    // eslint-disable-next-line @typescript-eslint/ban-types
                    const func = ((<any>this)[method] as Function);
                    func(path, async (c: Context) => {
                        const response = await execute(c, extras, callback, options, this._after);
                        return c.json(response, 200);
                    });
                    this.print(`configuring route /${method.toUpperCase()} ${path}`);
                }
            }
        }

        // start the server
        return new Promise((resolve) => {
            const server = serve({fetch: this.fetch, port: this.port}, () => {
                setTimeout(async () => {
                    if(callback) await callback(this.port);
                    resolve(this.port);
                }, 500)
            });

            this.server = server;
        });
    }

    /***********************************************************************
     * close the server
     * @param callback 
     **********************************************************************/
    close(callback?: ((arg: unknown) => void) | null | undefined){
        if(this.server && typeof callback === 'function') this.server.close(callback);
        else if(this.server) this.server.close();
    }

    /***********************************************************************
     * create a GET route
     **********************************************************************/
    Get(path: string, callback: TRouteLite, opts?: IRouteOptions | undefined) {
        this.routeList.push({
            method: 'get',
            path,
            callback,
            opts
        });
    }

    /***********************************************************************
     * create a PUT route
     **********************************************************************/
    Put(path: string, callback: TRouteLite, opts?: IRouteOptions | undefined) {
        this.routeList.push({
            method: 'put',
            path,
            callback,
            opts
        });
    }

    /***********************************************************************
     * create a POST route
     **********************************************************************/
    Post(path: string, callback: TRouteLite, opts?: IRouteOptions | undefined) {
        this.routeList.push({
            method: 'post',
            path,
            callback,
            opts
        });
    }

    /***********************************************************************
     * create a DELETE route
     **********************************************************************/
    Delete(path: string, callback: TRouteLite, opts?: IRouteOptions | undefined) {
        this.routeList.push({
            method: 'delete',
            path,
            callback,
            opts
        });
    }
}
