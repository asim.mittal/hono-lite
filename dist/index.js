"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HonoLite = exports.ErrorLite = void 0;
/* eslint-disable no-ex-assign */
/* eslint-disable @typescript-eslint/no-explicit-any */
const hono_1 = require("hono");
const node_server_1 = require("@hono/node-server");
__exportStar(require("hono"), exports);
/**********************************************************************
 * ErrorLite
 * this extends the Error class to specifically handle the types of errors
 * thrown within this server's routes
 **********************************************************************/
class ErrorLite extends Error {
    constructor(id, meta, statusCode = 500) {
        super(id);
        this.id = id;
        this.err = meta instanceof Error ? meta.message : meta;
        this.status = statusCode;
    }
    get error() {
        return {
            id: this.id,
            error: this.err,
            status: this.status
        };
    }
}
exports.ErrorLite = ErrorLite;
/***********************************************************************
 * reads the api key in the specified header, validates it against the
 * api key that is provided
 *
 * @param cxt - Hono context
 * @param header - the request header where the API key resides
 * @param key - value of the API key
 * @returns throws an error if the API key is invalid
 **********************************************************************/
const checkApiKey = (cxt, header, key) => {
    const keyInReq = cxt.req.header(header);
    if (key && key.trim().length > 0 && key === keyInReq)
        return true;
    throw new ErrorLite("invalid_api_key", "the api key in your headers is missing or invalid", 401);
};
/***********************************************************************
 * wrapper for the callback that runs the route's business logic
 * @param c - Hono context
 * @param extras - an object that will be delivered the callback
 * @param callback - route callback / business logic
 * @param options - route options
 * @returns the response / error from the execution of the callback
 **********************************************************************/
const execute = (c, extras, callback, options, afterCb) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (options && options.apiKeyValue && options.apiKeyValue.trim().length > 0) {
            const { apiKeyHeader = '', apiKeyValue = '' } = options;
            checkApiKey(c, apiKeyHeader, apiKeyValue);
        }
        const response = yield callback(c, extras);
        if (response instanceof ErrorLite)
            throw response;
        if (typeof afterCb === 'function')
            afterCb(c, { response });
        return response;
    }
    catch (e) {
        if (!(e instanceof ErrorLite)) {
            e = new ErrorLite((e === null || e === void 0 ? void 0 : e.id) || 'error', (e === null || e === void 0 ? void 0 : e.message) || e);
        }
        if (typeof afterCb === 'function')
            afterCb(c, { error: e });
        throw e;
    }
});
/***********************************************************************
 * validates the config options passed into the server
 * @param port - the port you want to run the server on
 * @param options - ConfigOptions for the server
 * @returns returns the validated options or throws an error
 **********************************************************************/
const validateConfigOptions = (port, options) => {
    if (!options)
        return undefined;
    const { apiKeyValue = undefined, secureAllRoutes = false } = options;
    const { apiKeyHeader = undefined } = options;
    if (apiKeyValue && apiKeyValue.trim().length === 0)
        throw new Error("invalid_api_key");
    if (apiKeyHeader && apiKeyHeader.trim().length === 0)
        throw new Error("invalid_api_header");
    if (apiKeyValue && !apiKeyHeader)
        options.apiKeyHeader = "x-api-key";
    if (apiKeyHeader && !apiKeyValue)
        throw new Error("api_key_not_defined");
    if (secureAllRoutes && (!apiKeyHeader || !apiKeyValue))
        throw new Error("cannot_secure_api");
    if (!port || port <= 0 || port > 65535)
        throw new Error("invalid_port");
    return options;
};
/***********************************************************************
 * HonoLite
 * the lite wrapper for Hono servers
 **********************************************************************/
class HonoLite extends hono_1.Hono {
    /***********************************************************************
     * constructor
     * @param port the port you want to run the server on
     * @param extras an object that will be passed into every route callback
     * @param options server config options
     **********************************************************************/
    constructor(port, options, extras = {}) {
        super();
        this.routeList = [];
        this.verbose = false;
        this.configOptions = validateConfigOptions(port, options);
        this.port = port;
        this.extras = extras;
        const { secureAllRoutes = false, apiKeyHeader = '', apiKeyValue = '' } = this.configOptions || {};
        if (secureAllRoutes === true) {
            this.use('*', (cxt, next) => __awaiter(this, void 0, void 0, function* () {
                checkApiKey(cxt, apiKeyHeader, apiKeyValue);
                yield next();
            }));
        }
        this.onError((error, context) => {
            var _a;
            return context.json(Object.assign({}, error), (_a = error === null || error === void 0 ? void 0 : error.status) !== null && _a !== void 0 ? _a : 500);
        });
        this.notFound((context) => {
            return context.json({ id: "not_found" }, 404);
        });
    }
    /************************************************************************
     * internal print function based on 'verbose' configuration
     ***********************************************************************/
    print(message, ...optionalParams) {
        if (this.verbose) {
            console.log(message, ...optionalParams);
        }
    }
    set before(callback) {
        this._before = callback;
    }
    set after(callback) {
        this._after = callback;
    }
    /***********************************************************************
     * add a key-value pair the 'extras' map
     * @param key key
     * @param value value to add in the extras
     **********************************************************************/
    addExtra(key, value) {
        if (!Object.prototype.hasOwnProperty.call(this.extras, key)) {
            this.extras[key] = value;
        }
    }
    /***********************************************************************
     * setter for the api key
     * will only set it if the initial config options supported this
     **********************************************************************/
    set apiKey(value) {
        if (!this.configOptions)
            throw new Error("config_options_unspecified");
        this.configOptions.apiKeyValue = value;
    }
    /***********************************************************************
     * start the server
     * @param callback a callback to invoke after server starts
     * @returns promise
     **********************************************************************/
    start(callback) {
        this.extras = Object.freeze(this.extras);
        // attach the pre-request middleware
        if (this._before) {
            this.use('*', this._before);
        }
        // attach all the routes
        if (this.routeList.length > 0) {
            for (const routeOpts of this.routeList) {
                const extras = this.extras;
                const { method, path, callback, opts } = routeOpts;
                const options = (opts === null || opts === void 0 ? void 0 : opts.useApiKey) ? this.configOptions : undefined;
                if (Object.prototype.hasOwnProperty.call(this, method)) {
                    // eslint-disable-next-line @typescript-eslint/ban-types
                    const func = this[method];
                    func(path, (c) => __awaiter(this, void 0, void 0, function* () {
                        const response = yield execute(c, extras, callback, options, this._after);
                        return c.json(response, 200);
                    }));
                    this.print(`configuring route /${method.toUpperCase()} ${path}`);
                }
            }
        }
        // start the server
        return new Promise((resolve) => {
            const server = (0, node_server_1.serve)({ fetch: this.fetch, port: this.port }, () => {
                setTimeout(() => __awaiter(this, void 0, void 0, function* () {
                    if (callback)
                        yield callback(this.port);
                    resolve(this.port);
                }), 500);
            });
            this.server = server;
        });
    }
    /***********************************************************************
     * close the server
     * @param callback
     **********************************************************************/
    close(callback) {
        if (this.server && typeof callback === 'function')
            this.server.close(callback);
        else if (this.server)
            this.server.close();
    }
    /***********************************************************************
     * create a GET route
     **********************************************************************/
    Get(path, callback, opts) {
        this.routeList.push({
            method: 'get',
            path,
            callback,
            opts
        });
    }
    /***********************************************************************
     * create a PUT route
     **********************************************************************/
    Put(path, callback, opts) {
        this.routeList.push({
            method: 'put',
            path,
            callback,
            opts
        });
    }
    /***********************************************************************
     * create a POST route
     **********************************************************************/
    Post(path, callback, opts) {
        this.routeList.push({
            method: 'post',
            path,
            callback,
            opts
        });
    }
    /***********************************************************************
     * create a DELETE route
     **********************************************************************/
    Delete(path, callback, opts) {
        this.routeList.push({
            method: 'delete',
            path,
            callback,
            opts
        });
    }
}
exports.HonoLite = HonoLite;
