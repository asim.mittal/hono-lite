"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable @typescript-eslint/no-explicit-any */
const globals_1 = require("@jest/globals");
const sample_server_1 = require("./sample-server");
const port = 15000;
const url = `http://127.0.0.1:${port}`;
const apiKey = "abc12def341-1290bc0";
const xtra = { someKey: "value" };
const server = (0, sample_server_1.createTestServer)(port, apiKey, xtra);
/******************************************************************************
 * server is defined here
******************************************************************************/
beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
    yield server.start(port => console.log("Server running on port:", port));
}), 10000);
afterAll(() => server.close());
/*******************************************************************************
 * Tests begin here
******************************************************************************/
(0, globals_1.describe)('test server', () => {
    const headers = { "x-api-key": apiKey };
    // test a basic get request
    it('test basic-get', () => __awaiter(void 0, void 0, void 0, function* () {
        const resp = yield fetch(`${url}/basic-get`);
        const { status = '' } = (yield resp.json());
        (0, globals_1.expect)(status).toBe(sample_server_1.ok.status);
    }));
    // test a basic get request with query parameters in the request url
    it('test get-with-params', () => __awaiter(void 0, void 0, void 0, function* () {
        const qParam = "some-message";
        const resp = yield fetch(`${url}/get-with-params?echo=${qParam}`);
        const { status = '', query = '' } = (yield resp.json());
        (0, globals_1.expect)(status).toBe(sample_server_1.ok.status);
        (0, globals_1.expect)(query).toBe(qParam);
    }));
    // test a put request with a JSON body
    it('test put-with-body', () => __awaiter(void 0, void 0, void 0, function* () {
        const content = { key: "some-value" };
        const resp = yield fetch(`${url}/put-with-body`, {
            method: "PUT",
            body: JSON.stringify(content),
            headers: { "Content-Type": "application/json" }
        });
        const { status = '', body = '' } = (yield resp.json());
        (0, globals_1.expect)(status).toBe(sample_server_1.ok.status);
        (0, globals_1.expect)(body['key']).toBe(content.key);
    }));
    // test an API route that throws an error
    it('test post-with-error', () => __awaiter(void 0, void 0, void 0, function* () {
        const r = yield fetch(`${url}/post-with-error`, { method: "POST" });
        (0, globals_1.expect)(r.ok).toBe(false);
    }));
    // test accessing "extras" from the server via an API
    it('test get-extras', () => __awaiter(void 0, void 0, void 0, function* () {
        const resp = yield fetch(`${url}/get-extras`);
        const { status = '', extras = undefined } = (yield resp.json());
        (0, globals_1.expect)(status).toBe(sample_server_1.ok.status);
        (0, globals_1.expect)(extras).toBeDefined();
        (0, globals_1.expect)(extras).toHaveProperty("someKey");
    }));
    // test editing "extras" from the server via an API
    it('test edit-extras', () => __awaiter(void 0, void 0, void 0, function* () {
        const r = yield fetch(`${url}/edit-extras`);
        (0, globals_1.expect)(r.ok).toBe(false);
    }));
    // test a secure api route by providing the correct api key
    it('test get-with-api-key', () => __awaiter(void 0, void 0, void 0, function* () {
        const resp = yield fetch(`${url}/get-with-api-key`, { headers });
        const { status = '', extras = undefined } = (yield resp.json());
        (0, globals_1.expect)(status).toBe(sample_server_1.ok.status);
        (0, globals_1.expect)(extras).toBeDefined();
        (0, globals_1.expect)(extras).toHaveProperty("someKey");
    }));
    // test a secure api route by providing no api key
    it('test error-with-api-key', () => __awaiter(void 0, void 0, void 0, function* () {
        const r = yield fetch(`${url}/get-with-api-key`);
        (0, globals_1.expect)(r.ok).toBe(false);
    }));
});
