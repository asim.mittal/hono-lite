"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTestServer = exports.ok = void 0;
const index_1 = require("../index");
exports.ok = { status: "success" };
const createTestServer = (port, apiKey, xtra) => {
    const config = { apiKeyValue: apiKey };
    const server = new index_1.HonoLite(port, config, xtra);
    server.Get('/basic-get', () => {
        return Object.assign(Object.assign({}, exports.ok), { message: "/basic-get works" });
    });
    server.Get('/get-with-params', (cxt) => __awaiter(void 0, void 0, void 0, function* () {
        const q = yield cxt.req.query('echo');
        return Object.assign(Object.assign({}, exports.ok), { message: "/get-with-params works", query: q });
    }));
    server.Put('/put-with-body', (cxt) => __awaiter(void 0, void 0, void 0, function* () {
        const body = yield cxt.req.json();
        return Object.assign(Object.assign({}, exports.ok), { body });
    }));
    server.Post('/post-with-error', () => {
        throw new index_1.ErrorLite("some_error_id", "error message goes here");
    });
    server.Get('/get-extras', (cxt, extras) => {
        return Object.assign(Object.assign({}, exports.ok), { extras });
    });
    server.Get('/edit-extras', (cxt, extras) => {
        //attempt to alter the extras... will fail
        extras['some-new-key'] = "some new value";
        return Object.assign(Object.assign({}, exports.ok), { extras });
    });
    server.Get('/get-with-api-key', (cxt, extras) => {
        return Object.assign(Object.assign({}, exports.ok), { extras });
    }, { useApiKey: true });
    server.Get('/test-pre-middleware', (cxt, extras) => {
        console.log('-> in request', cxt.req.path);
        const custom = cxt.get('customProp');
        return Object.assign(Object.assign({}, exports.ok), { custom,
            extras });
    }, { useApiKey: true });
    server.before = (c, next) => __awaiter(void 0, void 0, void 0, function* () {
        console.log('\n-> before', c.req.path);
        c.set('customProp', 'custom-value');
        yield next();
    });
    server.after = (c, r) => {
        const { response = null, error = null } = r;
        console.log('-> after |', c.req.path, error ? `error` : 'resp', ':', error || response);
    };
    return server;
};
exports.createTestServer = createTestServer;
if (require.main === module) {
    const server = (0, exports.createTestServer)(15000, "abc12def341-1290bc0", { test: "value" });
    server.start()
        .then(port => console.log(`Server is running on port: ${port}`))
        .catch(reason => server.close(() => console.log(reason)));
    const cleanup = () => server.close(() => console.log("Server closed"));
    process.on('exit', cleanup);
    process.on('SIGINT', cleanup);
}
