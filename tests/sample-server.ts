import { HonoLite, ErrorLite, Context, Next } from "../index";

export const ok = {status: "success"};

export const createTestServer = (port: number, apiKey?: string, xtra?: object) => {
  const config = { apiKeyValue: apiKey };
  const server = new HonoLite(port, config, xtra);
  
  server.Get('/basic-get', () => {
    return {
      ...ok,
      message: "/basic-get works"
    }
  });

  server.Get('/get-with-params', async (cxt) => {
    const q = await cxt.req.query('echo');
    return {
      ...ok,
      message: "/get-with-params works",
      query: q
    };
  });

  server.Put('/put-with-body', async (cxt) => {
    const body = await cxt.req.json();
    return {
      ...ok,
      body
    };
  });

  server.Post('/post-with-error', () => {
    throw new ErrorLite("some_error_id", "error message goes here");
  });

  server.Get('/get-extras', (cxt, extras) => {
    return {
      ...ok,
      extras
    }
  });

  server.Get('/edit-extras', (cxt, extras) => {
    //attempt to alter the extras... will fail
    extras['some-new-key'] = "some new value";
    return {
      ...ok,
      extras
    }
  });

  server.Get('/get-with-api-key', (cxt, extras) => {
    return {
      ...ok,
      extras
    }
  }, {useApiKey: true});

  server.Get('/test-pre-middleware', (cxt, extras) => {
    console.log('-> in request', cxt.req.path);
    const custom = cxt.get('customProp');
    
    return {
      ...ok,
      custom,
      extras
    }
  }, {useApiKey: true});

  server.before = async (c: Context, next: Next) => {
    console.log('\n-> before', c.req.path);
    c.set('customProp', 'custom-value');
    await next();
  };

  server.after = (c: Context, r: unknown) => {
    const {response = null, error = null} = <{ response:object, error: object }>r;
    console.log('-> after |', c.req.path, error ? `error`: 'resp', ':', error || response);
  }

  return server;
}

if(require.main === module){
  const server = createTestServer(15000, "abc12def341-1290bc0", {test: "value"});

  server.start()
    .then(port => console.log(`Server is running on port: ${port}`))
    .catch(reason => server.close(() => console.log(reason)));

  const cleanup = () => server.close(() => console.log("Server closed"));
  process.on('exit', cleanup);
  process.on('SIGINT', cleanup);
}