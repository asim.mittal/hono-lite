/* eslint-disable @typescript-eslint/no-explicit-any */
import { describe, expect } from '@jest/globals';
import { createTestServer, ok } from './sample-server';

const port = 15000;
const url = `http://127.0.0.1:${port}`;
const apiKey = "abc12def341-1290bc0"
const xtra = {someKey: "value"};

const server = createTestServer(port, apiKey, xtra);
  
/******************************************************************************
 * server is defined here
******************************************************************************/
beforeAll(async () => {
  await server.start(port => console.log("Server running on port:", port));
}, 10000);

afterAll(() => server.close());

/*******************************************************************************
 * Tests begin here
******************************************************************************/
describe('test server', () => {

  const headers = {"x-api-key": apiKey};

  // test a basic get request
  it('test basic-get', async () => {
    const resp = await fetch(`${url}/basic-get`);
    const {status = ''} = <any>(await resp.json());
    expect(status).toBe(ok.status);
  });

  // test a basic get request with query parameters in the request url
  it('test get-with-params', async () => {
    const qParam = "some-message";
    const resp = await fetch(`${url}/get-with-params?echo=${qParam}`);
    const {status = '', query = ''} = <any>(await resp.json());
    expect(status).toBe(ok.status);
    expect(query).toBe(qParam)
  });

  // test a put request with a JSON body
  it('test put-with-body', async () => {
    const content = {key: "some-value"};
    const resp = await fetch(`${url}/put-with-body`, {
      method: "PUT", 
      body: JSON.stringify(content), 
      headers: {"Content-Type": "application/json"}
    });
    
    const {status = '', body = ''} = <any>(await resp.json());
    expect(status).toBe(ok.status);
    expect(body['key']).toBe(content.key);
  });

  // test an API route that throws an error
  it('test post-with-error', async () => {
    const r = await fetch(`${url}/post-with-error`, { method: "POST" });
    expect(r.ok).toBe(false);
  });

  // test accessing "extras" from the server via an API
  it('test get-extras', async () => {
    const resp = await fetch(`${url}/get-extras`);
    const {status = '', extras = undefined} = <any>(await resp.json());
    expect(status).toBe(ok.status);
    expect(extras).toBeDefined();
    expect(extras).toHaveProperty("someKey");
  });

  // test editing "extras" from the server via an API
  it('test edit-extras', async () => {
    const r = await fetch(`${url}/edit-extras`);
    expect(r.ok).toBe(false);
  });

  // test a secure api route by providing the correct api key
  it('test get-with-api-key', async () => {
    const resp = await fetch(`${url}/get-with-api-key`, { headers });
    const {status = '', extras = undefined} = <any>(await resp.json());
    expect(status).toBe(ok.status);
    expect(extras).toBeDefined();
    expect(extras).toHaveProperty("someKey");
  });

  // test a secure api route by providing no api key
  it('test error-with-api-key', async () => {
    const r = await fetch(`${url}/get-with-api-key`);
    expect(r.ok).toBe(false);
  });
});